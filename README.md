[![pipeline status](https://gitlab.com/wpdesk/predators/library/wp-canada-post-shipping-method/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/wp-canada-post-shipping-method/pipelines)
[![coverage report](https://gitlab.com/wpdesk/predators/library/wp-canada-post-shipping-method/badges/master/coverage.svg?job=unit+test+lastest+coverage)](https://gitlab.com/wpdesk/wp-canada-post-shipping-method/commits/master)
[![Latest Stable Version](https://poser.pugx.org/wpdesk/wp-canada-post-shipping-method/v/stable)](https://packagist.org/packages/wpdesk/wp-canada-post-shipping-method)
[![Total Downloads](https://poser.pugx.org/wpdesk/wp-canada-post-shipping-method/downloads)](https://packagist.org/packages/wpdesk/wp-canada-post-shipping-method)
[![License](https://poser.pugx.org/wpdesk/wp-canada-post-shipping-method/license)](https://packagist.org/packages/wpdesk/wp-canada-post-shipping-method)

# Canada Post Shipping Method

Allows to integrate WooCommerce shipping methods interface with Canada Post rates calculation mechanism and services.

This library uses the following:
- https://gitlab.com/wpdesk/predators/library/abstract-shipping
- https://gitlab.com/wpdesk/wp-woocommerce-shipping
- https://gitlab.com/wpdesk/wp-canada-post-shipping-method
- https://gitlab.com/wpdesk/predators/library/canada-post-shipping-service

## Requirements

PHP 7.0 or later.

## Installation via Composer

In order to install the bindings via [Composer](http://getcomposer.org/) run the following command:

```bash
composer require wpdesk/wp-canada-post-shipping-method
```

## Example usage

```php
<?php

...
	public function hooks() {
		add_filter( 'woocommerce_shipping_methods', array( $this, 'add_canada_post_shipping_method' ) );
	}

	/**
	 * Adds shipping method to Woocommerce.
	 *
	 * @param array $methods Methods.
	 *
	 * @return array
	 */
	public function add_canada_post_shipping_method( $methods ) {
		$methods['flexible_shipping_canada_post'] = \WPDesk\WooCommerceShipping\CanadaPost\CanadaPostShippingMethod::class;

		return $methods;
	}

...
