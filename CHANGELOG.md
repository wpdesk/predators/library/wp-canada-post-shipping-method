## [1.3.0] - 2023-05-15
### Removed
- create_sender_address, sender address is created in wp-woocommerce-shipping library

## [1.2.0] - 2022-08-16
### Added
- en_CA, pl_PL translators

## [1.1.0] - 2022-07-07
### Added
- rates cache

## [1.0.4] - 2022-05-10
### Fixed
- zone locations
- texts
- meta data interpreters

## [1.0.1] - 2022-05-05
### Fixed
- docs link

## [1.0.0] - 2022-05-04
### Added
- initial version
